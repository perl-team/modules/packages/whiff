whiff (0.008-1) unstable; urgency=medium

  * Team upload.

  [ Florian Schlichting ]
  * Import upstream version 0.007.
  * Drop spelling.patch, applied upstream

  [ Debian Janitor ]
  * Apply multi-arch hints.
    + whiff: Add Multi-Arch: foreign.

  [ gregor herrmann ]
  * Import upstream version 0.008.
  * Update upstream email address.
  * Update debian/upstream/metadata.
  * Declare compliance with Debian Policy 4.6.2.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Wed, 04 Jan 2023 18:24:16 +0100

whiff (0.006-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Brian Cassidy from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Florian Schlichting ]
  * Import upstream version 0.006.
  * Update upstream contact details
  * Bump dh compat to level 13
  * Add Rules-Requires-Root: no
  * Drop alternative dependency on Test::More (>= 0.96), satisfied by perl
  * Declare compliance with Debian Policy 4.6.0
  * Add spelling.patch

 -- Florian Schlichting <fsfs@debian.org>  Tue, 24 Aug 2021 16:54:25 +0800

whiff (0.005-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Fri, 08 Jan 2021 16:32:48 +0100

whiff (0.005-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * Strip trailing slash from metacpan URLs.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Thu, 29 May 2014 19:38:22 +0200

whiff (0.003-1) unstable; urgency=low

  [ Damyan Ivanov ]
  * New Upstream Version 0.002
  * move VCS-* from apps/ to packages/

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.
  * debian/watch: update to ignore development releases.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Florian Schlichting ]
  * Import Upstream version 0.003
  * Remove copyright paragraph for Module::Install, which was dropped
  * Add build-dependency on Test::More 0.96
  * Bump Standards-Version to 3.9.4 (update to copyright-format 1.0, directly
    link GPL-1)
  * Bump dh compatibility to level 8 (no changes necessary)
  * Switch to source format 3.0 (quilt)
  * Switch to short-form debian/rules
  * Update Vcs-* fields to their canonical value
  * Add myself to uploaders and copyright

 -- Florian Schlichting <fsfs@debian.org>  Mon, 26 Aug 2013 20:38:04 +0200

whiff (0.001-1) unstable; urgency=low

  * Initial Release. (Closes: #511225)

 -- Brian Cassidy <brian.cassidy@gmail.com>  Mon, 12 Jan 2009 17:11:35 -0400
